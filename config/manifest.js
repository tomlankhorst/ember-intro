/* eslint-env node */
'use strict';

module.exports = function(/* environment, appConfig */) {
  // See https://github.com/san650/ember-web-app#documentation for a list of
  // supported properties

  return {
    name: "workshop",
    short_name: "workshop",
    description: "",
    start_url: "/",
    display: "standalone",
    background_color: "#ff466b",
    theme_color: "#ffbba8",
    icons: [
    ],
    ms: {
      tileColor: '#ffa65b'
    }
  };
}
