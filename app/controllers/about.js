import Controller from '@ember/controller';

export default Controller.extend({
  clicked: false,

  actions: {
    didClick(){
      this.set('clicked', true);
    }
  }
});
