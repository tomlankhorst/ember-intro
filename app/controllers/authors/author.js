import Controller from '@ember/controller';
import {task, timeout} from 'ember-concurrency';

export default Controller.extend({
  init(){
    this._super(...arguments);

    console.log('init');
    this.get('fetchBooks').perform();
  },

  fetchBooks: task(function*(){
      console.log('fetching');
      yield timeout(5000);

      const books = yield this.model.get('books');
      this.set('books', books);
  }).restartable(),
});
